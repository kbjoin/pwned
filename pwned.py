import sys
import hashlib
import requests
import re


def print_help():
    print('-----Help file for script-----\n'
          '\n'
          '--General Syntax--'
          'python3 pwned.py [password|sha1]\n'
          'Password or sha1 can be entered as a parameter\n'
          '\n'
          '--Example--\n'
          '\n'
          'python3 pwned.py <password>\n'
          '\n'
          'python3 pwned.py <sha1>\n'
          '\n'
          'keep the command from the bash history by adding a space before the command\n'
          '\n'
          '-----End of Help-----\n')
    exit(0)

def main():
    # Check for a help file request
    if len(sys.argv) == 1 or sys.argv.__contains__('-h'):
        print_help()

    # Setting a default state for the variable
    pw_compromised = False

    # Detect if the string passed is a password or a hash
    input = sys.argv[1]
    if len(input) == 40:
        if re.match("^[a-fA-F0-9]*$", input) is None:
            hash_object = hashlib.sha1(input.encode('utf-8'))
            pw_sha1 = hash_object.hexdigest().upper()
        else:
            pw_sha1 = input.upper()
    else:
        hash_object = hashlib.sha1(input.encode('utf-8'))
        pw_sha1 = hash_object.hexdigest().upper()
    # Sending the first five of the hash
    first_five = pw_sha1[0:5]
    keys = (requests.get('https://api.pwnedpasswords.com/range/' + first_five)).text
    # Checking the results for a password match
    for line in keys.splitlines():
        hash = first_five + line.split(':')[0]
        if hash == pw_sha1:
            print('Password has been found ' + line.split(':')[1] + ' times in known breaches')
            pw_compromised = True
    if pw_compromised == False:
        print('Password was not found in known breaches.')

if __name__ == "__main__":
    main()
