# pwned

Basic version of checking passwords against the have I been pwned API

Script will take either sha1 or the password as input.

Keep the command from the bash history by adding a space before the command

#  Requires
Python 3 (tested on 3.8.3)

Requests - https://3.python-requests.org/

# General Syntax
`python3 pwned.py [password|sha1]`

# Example
`python3 pwned.py [password]`

`python3 pwned.py [sha1]`

# Output
```
$ python3 pwned.py password1
Password has been found 2413945 times in known breaches
```

```
$ python3 pwned.py e38ad214943daad1d64c102faec29de4afe9da3d
Password has been found 2413945 times in known breaches
```

# Tip
To calculate sha1 in bash, be sure to use -n so the new line is not passed to
the hashing algorhythm 

`echo -n 'pw' | sha1sum`